
=========
Install [Minikube](https://github.com/kubernetes/minikube)

Requirements deploy minikube and kvm as a driver at localhost.
------------
- ansible => 2.4
- python => 2.7
- os = ubuntu/debian
- git

```
ansible-playbook -i inventory/hosts deploy-env-and-dependencies.yml -vvvvvv
```

Role Variables.
--------------

See [defaults/main.yml](defaults/main.yml)


Playbook localhost deploy Minikube environment with Helm and kubectl.
----------------

```yml
- hosts: localhost
  connection: local
  become: yes
  become_user: root
  become_method: sudo
  roles:
    - role: '{{playbook_dir}}'
      minikube_install_drivers:
        - kvm2
```

Deploy EFK (Elasticsearch, Fluentd, Kibana) as a log agregator stack without ansible via tiller as a third-party tools log agregator for any services which located at Kubernetes. This solutions is agnostic.
-------

1.) Pre-installed Minikube.

2.) Install Kubernetes package manager [HELM](https://docs.helm.sh/using_helm/).

3.) Init helm which will deploy tiller into minikube without RBAC.

```sh
helm init
kubectl get po,svc -n kube-system ###Check tiller service and pod
```

4.) Clone repo with helm charts.

```sh
git clone https://madlamer@bitbucket.org/madlamers/minikube-helm.git

```

5.) List of charts.

```sh
helm-charts/elasticsearch
├ Chart.yaml
├ templates
│   ├ deployment.yaml
│   ├ _helpers.tpl
│   ├ ingress.yaml
│   └ service.yaml
└ values.yaml
helm-charts/fluentd
├ Chart.yaml
├ templates
│   ├ deployment.yaml
│   ├ fluentd-config.yaml
│   └ _helpers.tpl
└ values.yaml
helm-charts/kibana
├ Chart.yaml
├ templates
│   ├ deployment.yaml
│   ├ _helpers.tpl
│   ├ ingress.yaml
│   └ service.yaml
└ values.yaml
```

6.) Set up charts of EFK stack.

 - Install and remove if need chart

```sh
cd helm-charts
helm install kibana -n kibana
helm install elasticsearch -n elasticsearch
helm install fluentd -n fluentd

helm del --purge fluentd
helm del --purge elasticsearch
helm del --purge kibana
```

- Kibana and Elasticsearch located at default namespace. Fluentd at kube-system.


============ AWS and log agregation steps.

1. Create IAM policy and role allowing read/write access to CloudWatch logs.

2. Set up via helm [fluentd-cloudwatch](https://github.com/helm/charts/tree/master/incubator/fluentd-cloudwatch)

3. Attach IAM role to pods via kube2iam with Fluentd.
